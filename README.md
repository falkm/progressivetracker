# Progressive Tracker
Project homepage: http://mielke-bio.info/falk/tracking

Publication:
Maja Mielke, Peter Aerts, Chris Van Ginneken, Sam Van Wassenbergh, Falk Mielke (2020):
*Progressive tracking: a novel procedure to facilitate manual digitization of videos.* Biology Open; DOI: 10.1242/bio.055962


This tool is used to track arbitrary points in a video.
Special Feature: Progressive Tracking!
<br>    --> Press [space] to play/pause video and follow a point of interest with the mouse cursor.
<br>        Mouse position will be tracked continuously.
<br>        No clicking required!

To select mode (i.e. point), press numbers 1-0; 
Modes are defined in the code (dictionary).

Currently, the video has to be stored as an image sequence in a folder.
Any video can easily be transformed into an image sequence via ffmpeg:
<br>    ffmpeg -i '<file.mpg>'' -r 1/1 '<folder>/$filename%04d.png'
<br>    (https://stackoverflow.com/questions/10957412/fastest-way-to-extract-frames-using-ffmpeg)

You can zoom or pan with the tools from the matplotlib toolbar. 

The following shortcuts will be useful:
<br>    - [escape]|[q]: quit
<br>    - [F1]: show/hide help
<br>    - [space]: play/pause (progressive tracking, [ctrl] to play backwards)
<br>    - [left]/[right]: jump 1 frame back/forward
<br>    - [shift]+[left]/[right]: jump 10 frames
<br>    - [home]: jump to start
<br>    - [+]/[-]: accelerate/decelerate; max speed is limited by computer
<br>    - [s]/[l]: save/load
<br>    - [shift]+[s]: save as
<br>    - [1-9]: tracking modes
<br>    - [down]/[up]: previous/next tracking mode
<br>    - [0]: playback mode (no tracking)
<br>    - [i]/[o]/[p]: zoom out/center view/zoom in
<br>    - [k]:(de-)activate pan/zoom tool
<br>    - [j]/[l]: shift view left/right ([shift] increase pixel step, [ctrl] shift view up/down)
<br>    - mouse wheel will adjust playback speed
<br>    - click on the progress bar at the bottom to jump to a frame


To make tracking less dull, I'm collecting a playlist with music that has some relation to tracking. Your suggestions are welcome!
Here is a start:
<br>    - Jack Johnson: "Inaudible Melodies", https://www.youtube.com/watch?v=V8ZAr7CYo10


Happy tracking!
